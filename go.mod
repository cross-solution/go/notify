module cross-solution.de/notify

go 1.13

require (
	github.com/golang/protobuf v1.5.2
	github.com/micro/go-micro v1.18.0
	gitlab.com/cross-solution/go/notify v0.0.0-20210908093535-96165a2728ef
)
